import sys
import time
import sqlite3
import pathlib
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException 

mainPath = pathlib.Path(__file__).parent.absolute().parent.absolute()
chromedriverPathWin = str(mainPath) + r'/driver/chromedriver.exe'
chromedriverPathLin = str(mainPath) + r'/driver/chromedriver'
databasePath = str(mainPath.parent.absolute()) + r'/db.sqlite3'

url = 'http://127.0.0.1:8000/polls/1/'

class scripts_qa(unittest.TestCase):

    def setUp(self):
        chromePath = ''
        chrome_options = Options()
        chrome_options.add_argument("--start-maximized")
        print('SOY PLATAFORMA: ' + str(sys.platform))
        if sys.platform == 'win32':
            print('SOY WIN: ' + chromedriverPathWin)
            chromePath = chromedriverPathWin
        else:
            chromePath = chromedriverPathLin
            print('SOY OTRO SO: ' + chromedriverPathLin)

        self.driver = webdriver.Chrome(executable_path = chromePath, chrome_options=chrome_options)

    #Script 1) Al abrir la pagina, validar que las opciones de cada candidato esten presentes en pantalla
    def test_1(self):
        driver = self.driver
        driver.get(url)
        time.sleep(2)
        self.assertTrue(check_element_exists_by_id("choice1", driver))
        time.sleep(2)
        self.assertTrue(check_element_exists_by_id("choice2", driver))
        time.sleep(2)
        self.assertTrue(check_element_exists_by_id("choice3", driver))
        time.sleep(2)
        assert "Alguno(s) de los elementos no existe en pantalla"


    #Script 2) Votar por cada candidato y validar si la cantidad de votos incrementa en uno
    def test_2(self):
        notMuchVotes = getVotes("Not much")
        theSkyVotes = getVotes("The sky")
        laLaLaVotes = getVotes("La la la")
        print ("Not much votes = ", notMuchVotes)
        print ("The Sky votes = ", theSkyVotes)
        print ("La la la votes = ", laLaLaVotes)

        driver = self.driver
        driver.get(url)
        time.sleep(2)

        not_much_option = driver.find_element_by_id("choice1")
        btn_vote = driver.find_element_by_xpath("//input[@type='submit']")
        not_much_option.click()
        btn_vote.click()
        time.sleep(2)
        lbl_not_much = driver.find_element_by_xpath("//li[1]")
        self.assertEqual(lbl_not_much.text, "Not much -- " + str(notMuchVotes + 1) + " votes")
        driver.find_element_by_link_text("Vote again?").click()
        time.sleep(2)

        the_sky_option = driver.find_element_by_id("choice2")
        btn_vote = driver.find_element_by_xpath("//input[@type='submit']")
        the_sky_option.click()
        btn_vote.click()
        time.sleep(2)
        lbl_the_sky = driver.find_element_by_xpath("//li[2]")
        self.assertEqual(lbl_the_sky.text, "The sky -- " + str(theSkyVotes + 1) + " votes")
        driver.find_element_by_link_text("Vote again?").click()
        time.sleep(2)
        
        lalala_option = driver.find_element_by_id("choice3")
        btn_vote = driver.find_element_by_xpath("//input[@type='submit']")
        lalala_option.click()
        btn_vote.click()
        time.sleep(2)
        lbl_lalala = driver.find_element_by_xpath("//li[3]")
        self.assertEqual(lbl_lalala.text, "La la la -- " + str(laLaLaVotes + 1) + " votes")
        time.sleep(2)
        assert "Alguno(s) de los votos no fue contabilizado de manera correcta"


    #Script 3) Intentar votar sin seleccionar ninguna opcion y validar de que sea obligatorio seleccionar al menos una opcion
    def test_3(self):
        driver = self.driver
        driver.get(url)
        time.sleep(2)
        btn_vote = driver.find_element_by_xpath("//input[@type='submit']")
        btn_vote.click()
        lbl_required_message = driver.find_element_by_xpath("//strong")
        time.sleep(2)
        self.assertEqual(lbl_required_message.text, "You didn't select a choice.")
        time.sleep(2)
        assert "La validación no se realizo de manera adecuada"

    def tearDown(self):
        self.driver.close()

#Utilerias
def check_element_exists_by_id(id, driver):
    try:
        driver.find_element_by_id(id) 
        print("Element exist with id " + id)
    except NoSuchElementException:
        print("Element does not exist with id " + id)
        return False
    return True

#Obtener cantidad de votos de base de datos 
def getVotes(choiceText):
    votes = 0
    try:
        sqliteConnection = sqlite3.connect(databasePath)
        cursor = sqliteConnection.cursor()
        print("Connected to SQLite")

        sqlite_select_query = """SELECT id, choice_text, votes, question_id from polls_choice where choice_text = ?"""
        cursor.execute(sqlite_select_query, (choiceText,))
        record = cursor.fetchone()
        votes = record[2]
        cursor.close()
    except sqlite3.Error as error:
        print("Failed to read single row from sqlite table", error)
    finally:
        if sqliteConnection:
            sqliteConnection.close()
            print("The SQLite connection is closed")

    return votes

if __name__ == '__main__':
    unittest.main()


